# Welcome to Trade-NestJs 👋
![Version](https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000)
[![License: UNLICENSED](https://img.shields.io/badge/License-UNLICENSED-yellow.svg)](#)

## Install

```sh
npm install
```

## Usage

```sh
npm start
```

## Run tests

```sh
npm test
```

## Author

👤 **Gustavo Joshua de Sene**

* Website: Gustavo Joshua de Sene
* Github: [@JoshuaSene](https://github.com/JoshuaSene)
* LinkedIn: [@JoshuaSene](https://linkedin.com/in/JoshuaSene)

## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_