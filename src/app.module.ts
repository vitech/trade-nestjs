/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './users/user.module';
import { AuthModule } from './auth/auth.module';
import { RoleModule } from './role/role.module';
import { UserRolesModule } from './user-roles/user-roles.module';
import { ImovelModule } from './imovel/imovel.module';
import { ContractModule } from './contract/contract.module';

import { RoomModule } from './room/room.module';
import { SubRoomModule } from './sub-room/sub-room.module';
import { RoomPhotoModule } from './room-photo/room-photo.module';
import { SubRoomPhotoModule } from './sub-room-photo/sub-room-photo.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      context: ({ req }) => ({
        req
      })
    }),
    UserModule,
    AuthModule,
    RoleModule,
    UserRolesModule,
    ImovelModule,
    ContractModule,
    RoomModule,
    SubRoomModule,
    RoomPhotoModule,
    SubRoomPhotoModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
