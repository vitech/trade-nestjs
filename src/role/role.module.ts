import { Module } from '@nestjs/common';
import Role from './entities/role.entity';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { GqlAuthGuard } from 'src/auth/auth.guard';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Role])],
      resolvers: [
        {
          EntityClass: Role,
          DTOClass: Role,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: [Role]
})
export class RoleModule {}
