import { FilterableField } from '@nestjs-query/query-graphql';
import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from 'typeorm';

@ObjectType()
@Entity('role')
class Role {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;

  @Column()
    @FilterableField()
  name: string;

  @CreateDateColumn()
    @Field(()=> GraphQLISODateTime)
  created_at: Date;

  @CreateDateColumn()
    @Field(()=> GraphQLISODateTime)
  updated_at: Date;
}
export default Role;
