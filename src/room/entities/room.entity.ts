import {
  FilterableField
} from '@nestjs-query/query-graphql';
import {
  Field,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import {
  Imovel
} from 'src/imovel/entities/imovel.entity';
import RoomPhoto from 'src/room-photo/entities/room-photo.entity';
import { SubRoom } from 'src/sub-room/entities/sub-room.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  ObjectType,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

@Entity({
  name: 'room'
})
export class Room {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;
 
  @OneToMany(() => SubRoom,
  subRoom => subRoom.id,
) subRoom!: SubRoom[];


  @OneToMany(() => RoomPhoto,
  roomPhoto => roomPhoto.roomId,
  ) roomPhoto!: RoomPhoto[];

  @ManyToOne(
    (): ObjectType < Imovel > => Imovel,
    im => im.created_at, {
      onDelete: 'CASCADE',
      nullable: false
    },
  )
  @JoinColumn()
  imovel!: Imovel;

  @Column()
  @FilterableField()
  name: string;

  @Column()
  @FilterableField()
  measure: string;

  @Column()
  @FilterableField()
  condition: string;

  @Column({
    nullable: false
  })
  imovelId!: string;  

  @Column({
    default: true
  })
  @FilterableField()
  status: boolean;

  @UpdateDateColumn()
  @Field(() => GraphQLISODateTime)
  created_at: Date;

  @UpdateDateColumn()
  @Field(() => GraphQLISODateTime)
  updated_at: Date;


}
export default Room;