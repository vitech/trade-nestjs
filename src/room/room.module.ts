import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/auth.guard';
import { RoomDTO } from './dto/room.dto';
import { Room } from './entities/room.entity';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Room])],
      resolvers: [
        {
          EntityClass: Room,
          DTOClass: RoomDTO,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: []
})
export class RoomModule {}
