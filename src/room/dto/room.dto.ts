import {
    FilterableField,
    Relation
} from "@nestjs-query/query-graphql";
import {
    GraphQLISODateTime,
    ID,
    ObjectType
} from "@nestjs/graphql";
import {
    ImovelDto
} from "src/imovel/dto/imovel.dto";

@Relation('imovel', () => ImovelDto, {
    disableRemove: true
})
@ObjectType('Room')
export class RoomDTO {

    @FilterableField(() => ID)
    id!: string;
    
    @FilterableField()
    name!: string;

    @FilterableField()
    measure!: string;

    @FilterableField()
    condition!: string;

    @FilterableField()
    status!: boolean;

    @FilterableField()
    imovelId!: string;

    @FilterableField(() => GraphQLISODateTime)
    created_at!: Date;

    @FilterableField(() => GraphQLISODateTime)
    updated_at!: Date;


}