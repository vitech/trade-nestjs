import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/auth.guard';
import { RoomPhotoDTO } from './dto/roomPhoto.dto';

import RoomPhoto from './entities/room-photo.entity';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([RoomPhoto])],
      resolvers: [
        {
          EntityClass: RoomPhoto,
          DTOClass: RoomPhotoDTO,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: []
})
export class RoomPhotoModule {}
