import {
  FilterableField
} from '@nestjs-query/query-graphql';
import {
  Field,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import Room from 'src/room/entities/room.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  ObjectType,
   OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

@Entity({
  name: 'roomPhoto'
})
export class RoomPhoto {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;
 
  @ManyToOne(
    (): ObjectType < Room > => Room,
    rm => rm.roomPhoto, {
      onDelete: 'CASCADE',
      nullable: false
    },
  )
  @JoinColumn()
  room!: Room;

  @Column()
  @FilterableField()
  name: string;

  @Column()
  @FilterableField()
  file: string;

  @Column()
  @FilterableField()
  roomId: string;
 
  @Column({
    default: true
  })
  @FilterableField()
  status: boolean;

  @CreateDateColumn()
  @Field(() => GraphQLISODateTime)
  created_at: Date;

  @UpdateDateColumn()
  @Field(() => GraphQLISODateTime)
  updated_at: Date;


}
export default RoomPhoto;