import {
    Authorize,
    FilterableField,
    Relation
} from "@nestjs-query/query-graphql";
import {
    GraphQLISODateTime,
    ID,
    ObjectType
} from "@nestjs/graphql";
import { RoomDTO } from "src/room/dto/room.dto";
 

@Relation('room', () => RoomDTO, {
    disableRemove: true
})
@ObjectType('RoomPhoto')
// @Authorize({ })
export class RoomPhotoDTO {
    
    @FilterableField(() => ID)
    id!: string;

    @FilterableField()
    name!: string;

    @FilterableField()
    file!: string;

    @FilterableField()
    status!: boolean;

    @FilterableField()
    roomId!: string;

    @FilterableField(() => GraphQLISODateTime)
    created_at!: Date;

    @FilterableField(() => GraphQLISODateTime)
    updated_at!: Date;


}