import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateRoomPhotoInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
