import { CreateRoomPhotoInput } from './create-room-photo.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateRoomPhotoInput extends PartialType(CreateRoomPhotoInput) {
  @Field(() => Int)
  id: number;
}
