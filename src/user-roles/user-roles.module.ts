import { Module } from '@nestjs/common';
import { UserRolesService } from './user-roles.service';
import { UserRolesResolver } from './user-roles.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRole } from './user-role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserRole])],
  providers: [UserRolesResolver, UserRolesService]
})
export class UserRolesModule {}
