import { Resolver } from '@nestjs/graphql';
import { UserRolesService } from './user-roles.service';

@Resolver()
export class UserRolesResolver {
  constructor(private readonly userRolesService: UserRolesService) {}
}
