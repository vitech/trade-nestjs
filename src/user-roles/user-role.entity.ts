import { Field, HideField, ID, ObjectType } from '@nestjs/graphql';
import Role from 'src/role/entities/role.entity';
import { User } from 'src/users/user.entity';
import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

@ObjectType()
@Entity('user_role')
export class UserRole {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @ManyToOne((type) => Role, () => UserRole)
  @JoinColumn({ name: 'roleId', referencedColumnName: 'id' })
  roleId: Role;

  @ManyToOne((type) => User, () => UserRole)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  userId: User;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
