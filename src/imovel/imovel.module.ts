/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Imovel } from './entities/imovel.entity';
import { ImovelDto } from './dto/imovel.dto';
import { GqlAuthGuard } from 'src/auth/auth.guard';
@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Imovel])],
      resolvers: [
        {
          EntityClass: Imovel,
          DTOClass: ImovelDto,
          guards:  [GqlAuthGuard],
        }
      ]
    })
  ],
  providers: [Imovel]
})
export class ImovelModule {}
