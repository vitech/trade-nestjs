import {
  FilterableField
}

from '@nestjs-query/query-graphql';

import {
  Field,
}

from '@nestjs/graphql';
import Contract from 'src/contract/entities/contract.entity';
import Room from 'src/room/entities/room.entity';

import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
}

from 'typeorm';

@Entity({
    name: 'imovel'
  }

) export class Imovel {

  @PrimaryGeneratedColumn('uuid') @FilterableField() @Field() id: string;

  @OneToMany(() => Contract,
    contract => contract.imovel,
  ) cont!: Contract[];

  @OneToMany(() => Room,
    room => room.imovel,
  ) room!: Room[];

  @Column() @FilterableField() type: string;

  @Column() @FilterableField() finalidade: string;

  @FilterableField() @Column({
    type: 'text'
  }) descricao: string;

  @FilterableField() @Column({
    type: 'text'
  }) country: string;

  @FilterableField() @Column({
    type: 'text'
  }) state: string;

  @FilterableField() @Column({
    type: 'text'
  }) city: string;

  @FilterableField() @Column({
    type: 'text'
  }) district: string;

  @FilterableField() @Column({
    type: 'text'
  }) street: string;

  @FilterableField() @Column({
    type: 'text'
  }) zipCode: string;

  @FilterableField() @Column() number: string;

  @Column({
    default: 'ENABLED'
  }) @FilterableField() status: string;

  @FilterableField() @Column({
    type: 'text'
  }) complement: string;

  @FilterableField() @Column({
    type: 'text',
    nullable: true
  }) area: string;

  @FilterableField() @Column({
    type: 'text',
    nullable: true
  }) centGas: string;

  @Column({
    type: 'json',
    nullable: true
  }) infoReader: JSON;

  @Column({
    type: 'json',
    nullable: true
  }) infoKey: JSON;

  @FilterableField() @Column({
    type: 'boolean',
    nullable: true,
  }) corner: boolean;

  @CreateDateColumn() created_at: Date;

  @UpdateDateColumn() updated_at: Date;


}