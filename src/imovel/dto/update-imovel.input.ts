
import { InputType, Field, Int, PartialType, ID } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@InputType()
export class UpdateImovelInput{
  @FilterableField(() => ID)
  id!: string;

  @FilterableField()
  descricao!: string;

  @FilterableField()
  type!: string;

  @FilterableField()
  finalidade!: string;

  @FilterableField()
  country!: string;

  @FilterableField()
  state!: string;

  @FilterableField()
  city!: string;

  @FilterableField()
  district!: string;

  @FilterableField()
  street!: string;

  @FilterableField()
  zipCode!: string;

  @FilterableField()
  number!: string;

  @FilterableField()
  complement!: string;

  @FilterableField()
  chaveInfo!: string;

  @FilterableField()
  leituraInfo!: string;

}
