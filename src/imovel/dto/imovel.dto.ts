import {
  FilterableField
} from "@nestjs-query/query-graphql";
import {
  GraphQLISODateTime,
  ID,
  ObjectType
} from "@nestjs/graphql";


@ObjectType('Imovel')
export class ImovelDto {

  @FilterableField(() => ID)
  id!: string;

  @FilterableField()
  descricao!: string;

  @FilterableField()
  type!: string;

  @FilterableField()
  finalidade!: string;

  @FilterableField()
  country!: string;

  @FilterableField()
  state!: string;

  @FilterableField()
  city!: string;

  @FilterableField()
  district!: string;

  @FilterableField()
  street!: string;

  @FilterableField()
  zipCode!: string;

  @FilterableField()
  number!: string;

  @FilterableField()
  complement!: string;

  @FilterableField()
  infofKey!: string;

  @FilterableField()
  infoReader!: string;

  @FilterableField()
  area!: string;

  @FilterableField()
  corner!: boolean;

  @FilterableField()
  centGas!: string;

  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;


}