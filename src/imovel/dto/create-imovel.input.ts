import { FilterableField } from '@nestjs-query/query-graphql';
import { InputType, Field } from '@nestjs/graphql';
import Contract from 'src/contract/entities/contract.entity';
 
@InputType('CreateImovelInput')
export class CreateImovelInput {
  // descricao: string;
 
  @FilterableField()
  descricao!: string;

  @FilterableField()
  type!: string;

  @FilterableField()
  finalidade!: string;

  @FilterableField()
  country!: string;

  @FilterableField()
  state!: string;

  @FilterableField()
  city!: string;

  @FilterableField()
  district!: string;

  @FilterableField()
  street!: string;

  @FilterableField()
  zipCode!: string;

  @FilterableField()
  number!: string;

  @FilterableField()
  complement!: string;

  @FilterableField()
  chaveInfo!: string;

  @FilterableField()
  leituraInfo!: string;
 
}
