import { Field, ObjectType } from '@nestjs/graphql';
import Role from 'src/role/entities/role.entity';
import { User } from 'src/users/user.entity';
// import { Token } from '../token.entity';

@ObjectType()
export class AuthType {
  @Field(() => User)
  user: User;

  @Field(() => String)
  token: string;

  @Field(() => String)
  tokenStatus: string;

  @Field(() => String)
  userId: string;

  @Field(() => String)
  role: string;

  @Field(() => String)
  expires: string;
  newToken: AuthType | PromiseLike<AuthType>;
}
