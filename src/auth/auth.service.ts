import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { UserRole } from 'src/user-roles/user-role.entity';
// import { subscribe } from 'graphql';
import { User } from 'src/users/user.entity';
import { UserService } from 'src/users/user.service';
import { getConnection, getRepository } from 'typeorm';
import { AuthInput } from './dto/auth.input';
import { AuthType } from './dto/auth.type';
import { Token } from './token.entity';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService
  ) {}

  async validateUser(data: AuthInput): Promise<any> {
    const user = await this.userService.getUserByEmail(data.email);
    const tokenRepository = getRepository(Token);

    const verifyRole = await getConnection().query(
      `select *
      from user_role ur
      where  ur."userId" = '${user.id}'
      `
    );

    const selectRole = await getConnection().query(
      `SELECT *
        FROM role r
        WHERE r."id" = '${verifyRole[0].roleId}'`
    );

    const validPasssword = compareSync(data.password, user.password);

    if (!validPasssword) {
      throw new UnauthorizedException('Incorrect password');
    }

    const token = await this.jwtToken(user, verifyRole[0]);

    const dateNow: Date = new Date();
    dateNow.setMinutes(dateNow.getMinutes() + 5);

    const newToken = tokenRepository.create({
      token,
      tokenStatus: 'Active',
      userId: user.id,
      expires: dateNow
    });

    await tokenRepository.save(newToken);

    return {
      user,
      token,
      newToken,
      selectRole
    };
  }

  private async jwtToken(user: User, userRole: UserRole): Promise<string> {
    const payload = { username: user.name, sub: user.id, role: userRole.id };
    return this.jwtService.signAsync(payload);
  }
}
