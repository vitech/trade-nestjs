import { Args, Mutation, Resolver } from '@nestjs/graphql';
import Role from 'src/role/entities/role.entity';
import { AuthService } from './auth.service';
import { AuthInput } from './dto/auth.input';
import { AuthType } from './dto/auth.type';
import * as Roles from './auth.service'
import { UseGuards } from '@nestjs/common';
import { RolesGuard } from './auth.guard';

@UseGuards(RolesGuard)
@Resolver('Auth')
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => AuthType)
  public async login(@Args('data') data: AuthInput): Promise<AuthType> {
    const response = await this.authService.validateUser(data);

 
    return {
      role: response.selectRole[0].name,
      user: response.user,
      token: response.token,
      ...response.newToken,

      
    };
  }
}
