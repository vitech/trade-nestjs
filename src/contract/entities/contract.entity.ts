import { FilterableField } from '@nestjs-query/query-graphql';
import { Field, GraphQLISODateTime } from '@nestjs/graphql';
import { Imovel } from 'src/imovel/entities/imovel.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  JoinColumn,
  ManyToOne,
  ObjectType,
  UpdateDateColumn,
} from 'typeorm';

  
@Entity({name: 'contract'})
export class Contract {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;


  @ManyToOne(
    ():ObjectType<Imovel> => Imovel,
    im => im.cont,
    { onDelete: 'CASCADE', nullable: false },
  )
  @JoinColumn()
  imovel!: Imovel;

  @Column()
    @FilterableField()
  locador: string;

  @Column()
  @FilterableField()
  adm: string;

  @Column({ nullable: false })
  imovelId!: string;

  @Column({ nullable: false })
  code!: string;

  @Column()
  @FilterableField()
  locatorio: string;

  @Column()
    @FilterableField()
  testemunhaUm: string;

  @Column()
    @FilterableField()
  testemunhaDois: string;

  @Column({
    default: true
  })
  @FilterableField()
  status: boolean;

  @Column()
    @FilterableField()
  vistoriador: string;
 
  @CreateDateColumn()  
  @Field(()=> GraphQLISODateTime)
  created_at: Date;

  @UpdateDateColumn()
    @Field(()=> GraphQLISODateTime)
  updated_at: Date;

  
}
export default Contract;