import { Module } from '@nestjs/common';

import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import Contract from './entities/contract.entity';
import { ContractDTO } from './dto/contract.dto';
import { GqlAuthGuard } from 'src/auth/auth.guard';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Contract])],
      resolvers: [
        {
          EntityClass: Contract,
          DTOClass: ContractDTO,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: []
  // providers: [Contract]
})
export class ContractModule {}
