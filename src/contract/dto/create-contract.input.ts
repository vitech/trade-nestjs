import { FilterableField } from '@nestjs-query/query-graphql';
import { InputType, Int, Field } from '@nestjs/graphql';
import Contract from '../entities/contract.entity';

@InputType()
export class CreateContractInput {
  @Field(() => Contract)
  finalidade: string;

  @Field(() => Contract)
  testemunhaUm: string;

  @Field(() => Contract)
  testemunhaDois: string;

  @Field(() => Contract)
  locatorio: string;

  @Field(() => Contract)
  adm: string;

  @Field(() => Contract)
  locador: string;

  @Field(() => Contract)
  tipo: string;

  @Field(() => Contract)
  vistoriador: string;
  
  @FilterableField()
  imoveis!: string;

  @FilterableField()
  imovelId!: string;

}
