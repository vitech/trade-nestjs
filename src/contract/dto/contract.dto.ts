import { FilterableField, Relation } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
import { ImovelDto } from 'src/imovel/dto/imovel.dto';

@ObjectType('Contract')
@Relation('imovel', () => ImovelDto, { disableRemove: true })
export class ContractDTO {
  @FilterableField(() => ID)
  id!: string;

  @FilterableField()
  finalidade!: string;

  @FilterableField()
  locador!: string;

  @FilterableField()
  adm!: string;

  @FilterableField()
  locatorio!: string;

  @FilterableField()
  testemunhaUm!: string;

  @FilterableField()
  testemunhaDois!: string;

  @FilterableField()
  tipo!: string;

  @FilterableField()
  code!: string;

  @FilterableField()
  vistoriador!: string;

  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;

  @FilterableField()
  imovelId!: string;
}
