import { CreateContractInput } from './create-contract.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';
import Contract from '../entities/contract.entity';

@InputType()
export class UpdateContractInput{
  @Field(() => Int)
  id: number;

  @Field(() => Contract)
  finalidade: string;

  @Field(() => Contract)
  testemunhaUm: string;

  @Field(() => Contract)
  testemunhaDois: string;

  @Field(() => Contract)
  locatorio: string;

  @Field(() => Contract)
  adm: string;

  @Field(() => Contract)
  locador: string;

  @Field(() => Contract)
  tipo: string;

  @Field(() => Contract)
  vistoriador: string;
}
