import { FilterableField } from '@nestjs-query/query-graphql';
import { Field, Int } from '@nestjs/graphql';
import Room from 'src/room/entities/room.entity';
import { SubRoomPhoto } from 'src/sub-room-photo/entities/sub-room-photo.entity';
import {
  JoinColumn,
  ObjectType,
  ManyToOne,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  UpdateDateColumn,
  CreateDateColumn
} from 'typeorm';

@Entity({
  name: 'subRoom'
})
export class SubRoom {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;

  @Column()
  @FilterableField()
  name: string;

  @OneToMany(() => SubRoomPhoto, (subRoomPhoto) => subRoomPhoto.subRP)
  subRP!: SubRoom[];

  @ManyToOne((): ObjectType<Room> => Room, (rm) => rm.id, {
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn()
  room!: Room;

  @Column({
    default: true
  })
  @FilterableField()
  status: boolean;

  @Column({
    nullable: false
  })
  roomId!: string;

  @Column({
    type: 'json',
    nullable: true
  })
  component: JSON;

  @CreateDateColumn() created_at: Date;

  @UpdateDateColumn() updated_at: Date;
}
