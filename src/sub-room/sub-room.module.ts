import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/auth.guard';
import { SubRoomDTO } from './dto/subRoom.dto';
import { SubRoom } from './entities/sub-room.entity';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([SubRoom])],
      resolvers: [
        {
          EntityClass: SubRoom,
          DTOClass: SubRoomDTO,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: []
})
export class SubRoomModule {}
