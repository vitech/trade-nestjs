import { CreateSubRoomInput } from './create-sub-room.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateSubRoomInput extends PartialType(CreateSubRoomInput) {
  @Field(() => Int)
  id: number;
}
