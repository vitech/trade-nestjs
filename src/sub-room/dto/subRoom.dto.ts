import {
    FilterableField,
    Relation
} from "@nestjs-query/query-graphql";
import {
    GraphQLISODateTime,
    ID,
    ObjectType
} from "@nestjs/graphql";

import {
    RoomDTO
} from "src/room/dto/room.dto";

@Relation('room', () => RoomDTO, {
    disableRemove: true
})
@ObjectType('SubRoom')
export class SubRoomDTO {

    @FilterableField(() => ID)
    id!: string;
    
    @FilterableField()
    name!: string;

    @FilterableField()
    measure!: string;

    @FilterableField()
    condition!: string;

    @FilterableField()
    component!: string;

    @FilterableField()
    status!: boolean;

    @FilterableField()
    roomId!: string;

    @FilterableField(() => GraphQLISODateTime)
    created_at!: Date;

    @FilterableField(() => GraphQLISODateTime)
    updated_at!: Date;


}