import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { UserService } from './user.service';
import { UserResolver } from './user.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UseCreateMiddleware } from './middlweare/role.middlweare';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UserService, UserResolver]

})

export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(UseCreateMiddleware)
      .forRoutes('/graphql/')
  }
}