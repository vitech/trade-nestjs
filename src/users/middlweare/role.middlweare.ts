
import {
    Injectable,
    NestMiddleware,
    HttpException,
    HttpStatus,
  } from '@nestjs/common'
  import { Request, Response } from 'express'
 
  
  @Injectable()
  export class UseCreateMiddleware implements NestMiddleware {
    async use(req: Request, res: Response, next: Function) {
      try {
         
       
  
        next()
      } catch (err) {
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: err.message,
          },
          HttpStatus.BAD_REQUEST
        )
      }
    }
  }