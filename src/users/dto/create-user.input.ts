import { InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class CreateUserInput {
  @IsString()
  @IsNotEmpty({ message: 'This field is required' })
  name: string;

  @IsEmail()
  @IsNotEmpty({ message: 'This field is required' })
  email: string;

  @IsString()
  @IsNotEmpty({ message: 'This field is required' })
  document: string;

  @IsString()
  @IsNotEmpty({ message: 'Password is required' })
  password?: string;
}
