import { Field, HideField, ID, ObjectType } from '@nestjs/graphql';
import { IsEmail } from 'class-validator';
import { hashPasswordTransform } from 'src/common/helpers/crypto';
import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn
} from 'typeorm';

@ObjectType()
@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;
  
  @Column()
  name: string;

  @Column() 
  email: string;

  @Column()
  document: string;

  @Column({
    transformer: hashPasswordTransform
  })
  @HideField()
  password: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
