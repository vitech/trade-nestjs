import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './user.entity';
import { UserService } from './user.service';
import { GqlAuthGuard, RolesGuard } from './../auth/auth.guard';
import { InternalServerErrorException, Request, UseGuards } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';

@Resolver('User')
export class UserResolver {
  constructor(private userService: UserService) {}


  @UseGuards(RolesGuard)
  @UseGuards(GqlAuthGuard)
  @Query(() => [User])
  async users(@Request() req): Promise<User[]> {
 
    const users = await this.userService.findAllUsers();
    return users;
  }


  @UseGuards(GqlAuthGuard)
  @Query(() => User)
  async user(@Args('id') id: string): Promise<User> {
    const user = await this.userService.getUserById(id);
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => User)
  async userByEmail(@Args('email') email: string): Promise<User> {
    
    const user = await this.userService.getUserByEmail(email);
    return user;
  }

  
  @Mutation(() => User)
  async createUser(@Args('data') data: CreateUserInput): Promise<User> {
    
    const user = await this.userService.createUser(data);
    
 
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => User)
  async updateUser(
    @Args('id') id: string,
    @Args('data') data: UpdateUserInput
  ): Promise<User> {
    const user = this.userService.updateUser(id, data);
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Boolean)
  async deleteUser(@Args('id') id: string): Promise<boolean> {
    const deleted = await this.userService.deleteUser(id);
    return deleted;
  }
}
