import {
  Injectable,
  InternalServerErrorException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Any, getConnection, getRepository, Repository } from 'typeorm';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './user.entity';
import * as UserRole from './../user-roles/user-role.entity';
import { request } from 'express';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  async findAllUsers(): Promise<User[]> {
    const users = await this.userRepository.find();
    return users;
  }

  async getUserById(data: string): Promise<User> {
    const user = await this.userRepository.findOne(data);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async fById(id: string): Promise<User> {
    const user = this.userRepository.findOne(id);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async getUserByEmail(email: string): Promise<User> {
    const user = this.userRepository.findOne({ where: { email } });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async createUser(data: CreateUserInput): Promise<User> {
    const email = data.email;
    const checkUserExists = await getRepository(User).findOne({
      where: { email }
    });

    if (checkUserExists) {
      throw new InternalServerErrorException('Email address already used');
    }
    const user = this.userRepository.create(data);
    const userSaved = await this.userRepository.save(user);
    const userRoleRepository = getRepository(UserRole.UserRole);

    const selectRole = await getConnection().query(
      `SELECT *
        FROM role r
        WHERE name = 'USER'`
    );

    const insertRoles = selectRole[0].id;

    const userRoles = userRoleRepository.create({
      roleId: insertRoles,
      userId: userSaved
    });

    const usr = await userRoleRepository.save(userRoles);

    if (!userSaved) {
      throw new InternalServerErrorException('Problema para criar um usuário');
    }

    return userSaved;
  }

  async updateUser(id: string, data: UpdateUserInput): Promise<User> {
    const user = await this.getUserById(id);
    await this.userRepository.update(user, { ...data });
    const userUpdate = this.userRepository.create({ ...user, ...data });
    return userUpdate;
  }

  async deleteUser(id: string): Promise<boolean> {
    const user = await this.getUserById(id);
    const deleted = await this.userRepository.delete(user);

    if (deleted) {
      return true;
    }
    return false;
  }
}
