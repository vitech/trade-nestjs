import {
  FilterableField
} from '@nestjs-query/query-graphql';
import {
  Field,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import { SubRoom } from 'src/sub-room/entities/sub-room.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  ObjectType,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

@Entity({
  name: 'subRoomPhoto'
})
export class SubRoomPhoto {
  @PrimaryGeneratedColumn('uuid')
  @FilterableField()
  id: string;
 
  @ManyToOne(
    (): ObjectType < SubRoom > => SubRoom,
    sr => sr.subRP, {
      onDelete: 'CASCADE',
      nullable: false
    },
  )
  @JoinColumn()
  subRP!: SubRoom;

  @Column()
  @FilterableField()
  name: string;

  @Column()
  @FilterableField()
  file: string;
 
  @Column({
    default: true
  })
  @FilterableField()
  status: boolean;

  @CreateDateColumn()
  @Field(() => GraphQLISODateTime)
  created_at: Date;

  @UpdateDateColumn()
  @Field(() => GraphQLISODateTime)
  updated_at: Date;


}
export default SubRoomPhoto;