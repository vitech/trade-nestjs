import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateSubRoomPhotoInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
