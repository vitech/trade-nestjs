import {
    FilterableField,
    Relation
} from "@nestjs-query/query-graphql";
import {
    GraphQLISODateTime,
    ID,
    ObjectType
} from "@nestjs/graphql"; 
import { SubRoomDTO } from "src/sub-room/dto/subRoom.dto";

@Relation('subRoom', () => SubRoomDTO, {
    disableRemove: true
})
@ObjectType('SubRoomPhoto')
export class SubRoomPhotoDTO {
    @FilterableField(() => ID)
    id!: string;

    @FilterableField()
    name!: string;

    @FilterableField()
    file!: string;

    @FilterableField()
    status!: boolean;

    @FilterableField(() => GraphQLISODateTime)
    created_at!: Date;

    @FilterableField(() => GraphQLISODateTime)
    updated_at!: Date;

    @FilterableField()
    subRP!: string;

}