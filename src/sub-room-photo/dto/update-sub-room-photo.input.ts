import { CreateSubRoomPhotoInput } from './create-sub-room-photo.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateSubRoomPhotoInput extends PartialType(CreateSubRoomPhotoInput) {
  @Field(() => Int)
  id: number;
}
