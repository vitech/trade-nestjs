import { Module } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import SubRoomPhoto from './entities/sub-room-photo.entity';
import { SubRoomPhotoDTO } from './dto/subRoomPhoto.dto';
import { GqlAuthGuard } from 'src/auth/auth.guard';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([SubRoomPhoto])],
      resolvers: [
        {
          EntityClass: SubRoomPhoto,
          DTOClass: SubRoomPhotoDTO,
          guards: [GqlAuthGuard]
        }
      ]
    })
  ],
  providers: []
})
export class SubRoomPhotoModule {}
